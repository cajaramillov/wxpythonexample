from model.model import Student
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import MultipleResultsFound
from sqlalchemy.orm.exc import NoResultFound


class StudentController():

    def connect_database(self):
        engine = create_engine("mysql+pymysql://root:9867024@localhost/college")
        Session = sessionmaker(bind=engine)
        session = Session()
        return session

    def get_student(self, id_student):
        session = self.connect_database()

        try:
            student = session.query(Student).filter_by(id=id_student).one()
            session.close()
            return student

        except (MultipleResultsFound, NoResultFound):
            return None

    def get_all_students(self):
        session = self.connect_database()
        students = session.query(Student).all()
        session.close()
        return students

    def create_student(self, first_name, last_name, average):
        student = Student(first_name=first_name,
                          last_name=last_name,
                          average=average)
        session = self.connect_database()
        session.add(student)
        session.commit()
        session.close()

    def edit_student(self, id_student, data):
        session = self.connect_database()
        student = session.query(Student).filter_by(id=id_student).one()
        student.first_name = data['first_name']
        student.last_name = data['last_name']
        student.average = data['average']
        session.add(student)
        session.commit()
        session.close()

    def delete_student(self, id_student):
        session = self.connect_database()
        student = session.query(Student).filter_by(id=id_student).one()
        session.delete(student)
        session.commit()
        session.close()
