import wx
from wx import xrc
from controller.studentcontroller import StudentController
from controller.addstudent import AddStudentFrame

UPDATE_STUDENT = "Update Student"
DELETE_STUDENT = "Delete Student"


class MainMenuFrame(wx.Frame):
    def __init__(self, *args, **kw):
        super(MainMenuFrame, self).__init__(*args, **kw)
        self.res = xrc.XmlResource('../View/College.xrc')
        self.student_controller = StudentController()
        self.add_student_frame = None
        self.student_selected = None
        self.list_students = []
        self.frame = self.res.LoadFrame(None, 'MenuFrame')
        self.panel_menu = xrc.XRCCTRL(self.frame, 'PanelMenu')
        self.listbook_menu = xrc.XRCCTRL(self.panel_menu, 'ListbookMenu')
        self.panel_students = xrc.XRCCTRL(self.listbook_menu, 'PanelStudents')
        self.listctrl_students = xrc.XRCCTRL(self.panel_students, 'ListCtrlStudents')
        self.button_add_student = xrc.XRCCTRL(self.panel_students, 'ButtonAddStudent')
        self.load_columns_listctrl_student()
        self.load_data_listctrl_student()
        self.frame.Bind(wx.EVT_BUTTON, self.create_student, self.button_add_student)
        self.frame.Bind(wx.EVT_LIST_ITEM_SELECTED, self.list_students_selected, self.listctrl_students)
        self.frame.Show()

    def load_columns_listctrl_student(self):
        self.listctrl_students.InsertColumn(0, "id", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listctrl_students.InsertColumn(1, "First Name", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listctrl_students.InsertColumn(2, "Last Name", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listctrl_students.InsertColumn(3, "Average", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)

    def load_data_listctrl_student(self):
        self.list_students = self.student_controller.get_all_students()
        self.listctrl_students.DeleteAllItems()

        for student in self.list_students:
            self.listctrl_students.Append([student.id, student.first_name, student.last_name, student.average])

    def create_student(self, evt):
        self.add_student_frame = AddStudentFrame(self)

    def list_students_selected(self, evt):
        current_item = evt.GetIndex()
        self.student_selected = self.list_students[current_item]
        menu = wx.Menu()
        id_item_menu_update = wx.NewId()
        id_item_menu_delete = wx.NewId()
        menu.Append(id_item_menu_update, UPDATE_STUDENT)
        menu.Append(id_item_menu_delete, DELETE_STUDENT)
        self.frame.Bind(wx.EVT_MENU, self.popup_item_selected, id=id_item_menu_update)
        self.frame.Bind(wx.EVT_MENU, self.popup_item_selected, id=id_item_menu_delete)
        self.frame.PopupMenu(menu)
        menu.Destroy()

    def popup_item_selected(self, evt):
        id_item = evt.GetId()
        menu = evt.GetEventObject()
        menu_item = menu.FindItemById(id_item)

        if menu_item.GetLabel() == UPDATE_STUDENT:
            self.add_student_frame = AddStudentFrame(self, self.student_selected.id)

        elif menu_item.GetLabel() == DELETE_STUDENT:
            msg = "Is sure to delete the Student %s %s???" % (self.student_selected.first_name, self.student_selected.last_name)
            result = wx.MessageBox(msg, "Delete Student", wx.YES_NO | wx.ICON_EXCLAMATION)

            if result == wx.YES:
                self.student_controller.delete_student(self.student_selected.id)
                self.load_data_listctrl_student()


