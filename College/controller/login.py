import wx
from wx import xrc
from controller.usercontroller import UserController
from controller.mainmenu import MainMenuFrame


class LoginFrame(wx.Frame):

    def __init__(self, *args, **kw):
        super(LoginFrame, self).__init__(*args, **kw)
        self.res = xrc.XmlResource('../View/Login.xrc')
        self.user_controller = UserController()
        self.main_menu = None
        self.frame = self.res.LoadFrame(None, 'LoginFrame')
        self.panel = xrc.XRCCTRL(self.frame, 'LoginPanel')
        self.username = xrc.XRCCTRL(self.panel, 'TextCtrlUsername')
        self.password = xrc.XRCCTRL(self.panel, 'TextCtrlPassword')
        self.button = xrc.XRCCTRL(self.panel, 'ButtonLogin')
        self.frame.Bind(wx.EVT_BUTTON, self.validate_user, self.button)
        self.frame.Show()

    def validate_user(self, evt):
        username = self.username.GetValue()
        password = self.password.GetValue()

        if username and password:
            if self.user_controller.search_user(username, password):
                self.frame.Close()
                self.main_menu = MainMenuFrame()
            else:
                wx.MessageBox('User does not exists', 'Error', wx.OK | wx.ICON_ERROR)

        else:
            wx.MessageBox('fields not entered', 'Error', wx.OK | wx.ICON_ERROR)


if __name__ == '__main__':
    app = wx.App()
    frame = LoginFrame()
    app.MainLoop()
