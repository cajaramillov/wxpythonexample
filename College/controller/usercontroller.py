from model.model import User
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import MultipleResultsFound
from sqlalchemy.orm.exc import NoResultFound


class UserController():

    def connect_database(self):
        engine = create_engine("mysql+pymysql://root:9867024@localhost/college")
        Session = sessionmaker(bind=engine)
        session = Session()
        return session

    def create_user(self, username, password):
        user = User(username=username,
                    password=password)
        session = self.connect_database()
        session.add(user)
        session.commit()
        session.close()

    def get_user(self, username, password):
        session = self.connect_database()

        try:
            user = session.query(User).filter_by(username=username, password=password).one()
            session.close()
            return user

        except (MultipleResultsFound, NoResultFound):
            return None

    def get_all_users(self):
        session = self.connect_database()
        users = session.query(User).all()
        session.close()
        return users

    def search_user(self, username, password):
        session = self.connect_database()

        try:
            user = session.query(User).filter_by(username=username, password=password).one_or_none()
            session.close()

            if user is not None:
                return True
            return False

        except MultipleResultsFound:
            session.close()
            return False
