import wx
from wx import xrc
from controller.studentcontroller import StudentController


class AddStudentFrame(wx.Frame):

    def __init__(self, frame_father=None, id_student=None):
        super(AddStudentFrame, self).__init__()
        self.res = xrc.XmlResource('../View/AddStudent.xrc')
        self.student_controller = StudentController()
        self.id_student = id_student
        self.frame = self.res.LoadFrame(None, 'AddStudentFrame')
        self.panel = xrc.XRCCTRL(self.frame, 'AddStudentPanel')
        self.first_name = xrc.XRCCTRL(self.panel, 'TextCtrlFirstName')
        self.last_name = xrc.XRCCTRL(self.panel, 'TextCtrlLastName')
        self.average = xrc.XRCCTRL(self.panel, 'TextCtrlAverage')
        self.button_save = xrc.XRCCTRL(self.panel, 'wxID_SAVE')
        self.button_cancel = xrc.XRCCTRL(self.panel, 'wxID_CANCEL')

        if frame_father is not None:
            self.frame_father = frame_father

        if self.id_student is not None:
            self.load_data_student()

        self.frame.Bind(wx.EVT_BUTTON, self.close_frame, self.button_cancel)
        self.frame.Bind(wx.EVT_BUTTON, self.create_student, self.button_save)
        self.frame.Show()

    def close_frame(self, evt):
        self.frame.Close()

    def create_student(self, evt):
        first_name = self.first_name.GetValue()
        last_name = self.last_name.GetValue()
        average = self.average.GetValue()

        if first_name and last_name and average:
            if self.id_student is not None:
                data = {'first_name': first_name, 'last_name': last_name, 'average': average}
                self.student_controller.edit_student(self.id_student, data)
                self.load_data_student()

            else:
                self.student_controller.create_student(first_name, last_name, average)
                wx.MessageBox('The Student was created successfully', 'Information', wx.OK | wx.ICON_INFORMATION)
                self.clear_fields()

            self.frame_father.load_data_listctrl_student()

        else:
            wx.MessageBox('Fields not entered', 'Error', wx.OK | wx.ICON_ERROR)

    def clear_fields(self):
        self.first_name.Clear()
        self.last_name.Clear()
        self.average.Clear()

    def load_data_student(self):
        student = self.student_controller.get_student(self.id_student)
        self.first_name.SetValue(student.first_name)
        self.last_name.SetValue(student.last_name)
        self.average.SetValue(str(student.average))