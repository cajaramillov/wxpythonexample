from sqlalchemy import Column, create_engine
from sqlalchemy import Integer, ForeignKey, String, Float, Text
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine("mysql+pymysql://root:9867024@localhost/college")
DeclarativeBase = declarative_base(engine)
metadata = DeclarativeBase.metadata


class Student(DeclarativeBase):
    __tablename__ = "student"
    id = Column(Integer, primary_key=True, autoincrement=True)
    first_name = Column("first_name", String(50))
    last_name = Column("last_name", String(50))
    average = Column("average", Float())

    def __repr__(self):
        return "<Student: %s %s>" % (self.first_name, self.last_name)


class Curse(DeclarativeBase):
    __tablename__ = "curse"
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column("name", String(50))
    description = Column("description", Text())

    def __repr__(self):
        return "<Curse: %s>" % (self.name)


class CurseStudent(DeclarativeBase):
    __tablename__ = "cursestudent"
    id = Column(Integer, primary_key=True, autoincrement=True)
    student_id = Column(Integer, ForeignKey("student.id"))
    curse_id = Column(Integer, ForeignKey("curse.id"))


class User(DeclarativeBase):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column("username", String(50), unique=True)
    password = Column("password", String(50))

    def __repr__(self):
        return "<User: %s>" % (self.username)


metadata.create_all()